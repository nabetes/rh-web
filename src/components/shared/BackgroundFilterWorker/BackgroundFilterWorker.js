
import { CustomWorker } from '../CustomWorker'

class BackgroundFilterWorker {
    constructor(config) {
        config = Object.assign({
            id: "BackgroundFilterWorker", 
            name: "backgroundFilter",
            $$map: ""
        }, config);

        this.worker = new CustomWorker({
            id: config.id,
            name: config.name,
            content: [config.$$map, function () {
                function checkObject(item, filter) {
                    let flag = false,
                        itemKeys = Object.keys(item);

                    for (let index = 0, length = itemKeys.length, key, prop; index < length; index++) {
                        key = itemKeys[index];
                        prop = item[key] || -1;
                        if (typeof prop !== "string" && Object.keys(prop).length)
                            flag = checkObject(prop, filter);
                        else if (prop.toString().replace(/(\r\n|\n|\r)/gm, "").trim().toLowerCase().indexOf(filter) !== -1)
                            flag = true;

                        if (flag)
                            break;
                    }

                    return flag;
                }

                function $$do(data) {
                    let collection = data.collection,
                        length = collection.length,
                        filter = (data.filter || "").trim().toLowerCase(),
                        reportProgress = data.reportProgress,
                        progressPerItem = length && reportProgress ? 100 / length : 0,
                        result = [];

                    if (filter === "")
                        result = collection;
                    else {
                        for (let index = 0, item, currentProgress = 0; index < length; index++) {
                            if (reportProgress)
                                this.$$sendProgress(currentProgress += progressPerItem);
                            item = collection[index];
                            if (this.$$map)
                                item = this.$$map(item);
                            if (checkObject(item, filter))
                                result.push(item);
                        }
                    }
                    this.$$send(result);
                }
            }]
        });
    }

    dispose = function () {
        this.worker.dispose();
    }

    filter = function ({ collection, filter, onProgress }) {
        return this.worker.send({
            collection: collection,
            filter: filter,
            reportProgress: !!onProgress
        }, onProgress);
    }
};

export default BackgroundFilterWorker;