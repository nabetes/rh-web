const arrayPaginate = ({array, pageNumber, pageSize}) => {
    const init = (pageNumber - 1) * pageSize;
    const finish = pageNumber * pageSize;
    return array.slice(isNaN(init) ? 0 : init, isNaN(finish) ? 1 : finish);
};

export {
    arrayPaginate,
}
