/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "../../../../../node_modules/react";
import { stringFormat, } from '../../StringTools';
import style from './Pagination.module.scss'

const getCalculations = ({ currentPage, pageSize, rowsLength, visiblePageRange, onPageChage }) => {
    const firstRowNumber = (currentPage === 1 ? 1 : (((currentPage - 1) * pageSize) + 1));
    let lastRowNumber = firstRowNumber + pageSize - 1;

    if (rowsLength < lastRowNumber) {
        lastRowNumber = rowsLength;
    }

    const totalPages = getTotalPages({ rowsLength, pageSize });
    const { visibleRangeOfPages } = getPaginatevisibleRangeOfPages({ currentPage, totalPages, visiblePageRange });

    let displayPreviouButton = false;
    let displayNextButton = false;

    if (visibleRangeOfPages.length > 3) {
        displayPreviouButton = visibleRangeOfPages.length && (visibleRangeOfPages[0] - 1) > 0 && visibleRangeOfPages.indexOf(visibleRangeOfPages[0] - 1) === -1;
        displayNextButton = visibleRangeOfPages.length && visibleRangeOfPages[visibleRangeOfPages.length - 1] < totalPages;
    }

    return {
        displayPreviouButton,
        displayNextButton,
        lastRowNumber,
        firstRowNumber,
        visibleRangeOfPages,
        totalPages,
    };
}

const getTotalPages = ({ rowsLength, pageSize }) => {
    return (!rowsLength ? 1 : (rowsLength % pageSize === 0) ? rowsLength / pageSize
        : parseInt((rowsLength / pageSize) + 1));
}

const getPaginatevisibleRangeOfPages = ({ currentPage, totalPages, visiblePageRange }) => {
    var left = currentPage - visiblePageRange,
        right = currentPage + visiblePageRange + 1,
        visibleRangeOfPages = [];

    if (visiblePageRange)
        for (let i = 1; i <= totalPages; i++) {
            if (i >= left && i < right) visibleRangeOfPages.push(i);
        }
    else visibleRangeOfPages = [currentPage];

    return {
        visibleRangeOfPages
    };
}

export default function Pagination({
    rowsLength,
    pageSize,
    visiblePageRange,
    onPageChage,
    pageNumber,
}) {
    const [currentPage, setCurrentPage] = useState(pageNumber || 1);

    const {
        visibleRangeOfPages,
        firstRowNumber,
        lastRowNumber,
        totalPages,
        displayPreviouButton,
        displayNextButton,
    } = getCalculations({
        pageSize,
        rowsLength,
        currentPage: currentPage || 1,
        visiblePageRange: visiblePageRange || 3,
    });

    const goToPage = function (number) {
        if (number && number >= 1 && number <= totalPages && number !== currentPage) {
            setCurrentPage(number);
            onPageChage(number);
        }
    };

    useEffect(() => {
        onPageChage(pageNumber);
    }, []);

    useEffect(() => {
        setCurrentPage(pageNumber);
    }, [pageNumber]);

    return (
        rowsLength ?
            <span>
                <div className={style.pagination}>
                    <div className={style.navigation}>
                        {currentPage > 1 ?
                            <a className={style.previosButton}
                                onClick={() => goToPage(1)}>
                                &lt;&lt;
                            </a>
                            : ''}
                        <span>
                            {displayPreviouButton ?
                                <a onClick={() => goToPage(visibleRangeOfPages[0] - 1)}
                                    className={style.intermittentButton}
                                    title="See more">
                                    ...
                                </a>
                                : ''}

                            {visibleRangeOfPages.map((number) => {
                                return (
                                    <a key={number}
                                        onClick={() => goToPage(number)}
                                        className={`${style.button} ${number === currentPage ? style.currentPage : ''}`}>
                                        {number}
                                    </a>
                                )
                            })}

                            {displayNextButton ?
                                <a onClick={() => goToPage(visibleRangeOfPages[visibleRangeOfPages.length - 1] + 1)}
                                    className={style.intermittentButton}
                                    title="See more">
                                    ...
                                </a>
                                : ''}
                        </span>

                        {currentPage < totalPages ?
                            <a className={style.nextButton}
                                onClick={() => goToPage(totalPages)}>
                                &gt;&gt;
                            </a>
                            : ''
                        }
                    </div>
                    <div className={style.legend}>
                        {stringFormat({ strg: 'Displaying {0} - {1} of {2} records', parameters: [firstRowNumber, lastRowNumber, rowsLength] })}
                    </div>
                </div>
            </span>
            : <div className="dataTables_wrapper">
                <div>
                    There no records to display...
                </div>
            </div>
    );
}