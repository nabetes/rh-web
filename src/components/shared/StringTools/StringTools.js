const stringFormat = ({ strg, parameters }) => strg.replace(/{(\d+)}/g, (match, number) => parameters[number] || match);

const replaceAll = ({ strg, find, replace }) => strg.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace)

export {
    stringFormat,
    replaceAll,
}