import React from "react";
import style from './ApplicationsList.module.scss';
import { useApplicationsFilteredPaginated } from '../ApplicationsHook';

export default function ApplicationsList() {
    const [{ applicationsFilteredPaginated }] = useApplicationsFilteredPaginated();

    return applicationsFilteredPaginated.length ? (
        <table className={style.ApplicationsList}>
            <thead>
                <tr>
                    <th>name</th>
                    <th>email</th>
                    <th>birth date</th>
                    <th>year of experience</th>
                    <th>application date</th>
                    <th>status</th>
                </tr>
            </thead>
            <tbody>
                {applicationsFilteredPaginated.map((application) => {
                    return (
                        <tr className={style.row}
                            key={application.id}>
                            <th>{application.name}</th>
                            <th>{application.email}</th>
                            <th>{application.birth_date}</th>
                            <th>{application.year_of_experience}</th>
                            <th>{application.application_date}</th>
                            <th>{application.status}</th>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    ) : '';
}
