import globalHook from 'use-global-hook';
import React from 'react';

const initialStateApplications = {
    applications: [],
}

const initialStateApplicationsFiltered = {
    applicationsFiltered: [],
}

const initialStateApplicationsFilteredPaginated = {
    applicationsFilteredPaginated: [],
}

const useApplications = globalHook(React, initialStateApplications, {
    set: (store, state) => store.setState({ ...state }),
});

const useApplicationsFiltered = globalHook(React, initialStateApplicationsFiltered, {
    set: (store, state) => store.setState({ ...state }),
});

const useApplicationsFilteredPaginated = globalHook(React, initialStateApplicationsFilteredPaginated, {
    set: (store, state) => store.setState({ applicationsFilteredPaginated: [].concat(state.applicationsFilteredPaginated) }),
});

export {
    useApplications,
    useApplicationsFiltered,
    useApplicationsFilteredPaginated,
}

