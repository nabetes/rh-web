import React, { useEffect, useState } from "react";
import Filter from "./Filter";
import ApplicationsList from "./ApplicationsList";
import { useApplications, useApplicationsFiltered, useApplicationsFilteredPaginated } from './ApplicationsHook';
import BackgroundFilterWorker from '../shared/BackgroundFilterWorker';
import Pagination from "../shared/components/Pagination/";
import { arrayPaginate } from '../shared/ArrayTools';
import { getUrlParameters, setUrlParameters } from './Filter/FilterParameters';

const CANDITATES_URL = 'https://personio-fe-test.herokuapp.com/api/v1/candidates';
const filterTask = new BackgroundFilterWorker();

const getApplications = () => {
  return new Promise((resolve) => {
    fetch(CANDITATES_URL)
      .then(request => request.json())
      .then((request) => {
        if (request.error) throw request.error;
        resolve(request.data);
      }).catch((error) => {
        console.error(error);
        getApplications().then(resolve);
      });
  });
}

export default function Applications() {
  const [{ applications }, applicationsActions] = useApplications();
  const [{ applicationsFiltered }, applicationsFilteredActions] = useApplicationsFiltered();
  const [, useApplicationsFilteredPaginatedActions] = useApplicationsFilteredPaginated();

  const [loading, setLoading] = useState(true);
  const [, setPaginatedApplications] = useState([]);
  const pageSize = 10;
  const queryParameters = getUrlParameters();

  useEffect(() => {
    getApplications().then((source) => {
      applicationsActions.set({ applications: source });
      if (queryParameters.filter) filterTask.filter({
        collection: source, filter: queryParameters.filter
      }).then(filtered => {
        applicationsFilteredActions.set({ applicationsFiltered: filtered });

        setPaginatedApplications(arrayPaginate({
          array: filtered,
          pageNumber: 1,
          pageSize,
        }));

        setLoading(false);
      });
      else {
        applicationsFilteredActions.set({ applicationsFiltered: source });

        setPaginatedApplications(arrayPaginate({
          array: source,
          pageNumber: 1,
          pageSize,
        }));

        setLoading(false);
      }

      // destroy worker
      return filterTask.dispose;
    });
  }, []);

  const onPageChage = (pageNumber) => {
    setUrlParameters({ ...queryParameters, pageNumber });

    useApplicationsFilteredPaginatedActions.set({
      applicationsFilteredPaginated: arrayPaginate({
        array: applicationsFiltered,
        pageNumber,
        pageSize,
      })
    });
  };

  return (<div>
    <Filter readOnly={loading} />
    {applications.length ? <ApplicationsList /> : ''}
    {loading ? '' :
      <Pagination
        pageNumber={queryParameters.pageNumber}
        rowsLength={applicationsFiltered.length}
        pageSize={pageSize}
        visiblePageRange={3}
        onPageChage={onPageChage}
      />}
  </div>);
}
