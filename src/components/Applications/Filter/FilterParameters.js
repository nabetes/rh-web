const getUrlParameters = () => {
    let result = { filter: '', pageNumber: 1 };
    try {
        const getParameter = window.location.href.split('?')[1];
        if (!getParameter) return result;

        // eslint-disable-next-line no-undef
        result = JSON.parse(atob(decodeURIComponent(getParameter.trim())));
    } finally {
        return result;
    }
}

const setUrlParameters = ({ filter, pageNumber }) => {
    const parameter = btoa(JSON.stringify({ filter, pageNumber }));
    const url = `${window.location.protocol}//${window.location.host}${window.location.pathname}?${parameter}`;
    window.history.pushState({ path: url }, document.title, url);
}

export {
    getUrlParameters,
    setUrlParameters
};