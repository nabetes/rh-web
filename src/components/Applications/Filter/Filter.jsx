import React from "react";
import style from './Filter.module.scss';
import BackgroundFilterWorker from '../../shared/BackgroundFilterWorker';
import { useApplications, useApplicationsFiltered, useApplicationsFilteredPaginated } from '../ApplicationsHook';
import { DebounceInput } from 'react-debounce-input';
import { useEffect } from "react";
import { arrayPaginate } from '../../shared/ArrayTools';
import { setUrlParameters, getUrlParameters } from './FilterParameters';

const filterTask = new BackgroundFilterWorker();

export default function Filter({ readOnly }) {
    const [{ applications }] = useApplications();
    const [, applicationsFilteredActions] = useApplicationsFiltered();
    const [, applicationsFilteredPaginatedActions] = useApplicationsFilteredPaginated();
    const queryParameters = getUrlParameters();

    const paginateSource = ({ array, pageNumber }) => arrayPaginate({
        array,
        pageSize: 10,
        pageNumber,
    });

    const searchApplications = ({ filter, pageNumber }) => {
        setUrlParameters({ filter, pageNumber });

        filterTask
            .filter({ collection: applications, filter })
            .then(filtered => {
                applicationsFilteredActions.set({ applicationsFiltered: filtered });

                applicationsFilteredPaginatedActions.set({
                    applicationsFilteredPaginated: paginateSource({ array: filtered, pageNumber }),
                });
            })
    };

    useEffect(() => {
        const { filter, pageNumber } = queryParameters;
        searchApplications({ filter, pageNumber });
    }, []);

    return (applications.length ?
        <DebounceInput
            placeholder="Search..."
            readOnly={readOnly}
            className={style.Filter}
            minLength={2}
            debounceTimeout={300}
            value={queryParameters.filter}
            onChange={event => searchApplications({ filter: event.target.value, pageNumber: 1 })} />
        : <p className={style.FilterLoading}>...Loading</p>);
}
