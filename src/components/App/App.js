import React from 'react';
import style from './App.module.scss';
import ApplicationsLogo from "../../assets/icons/user-logo.svg";
import Applications from '../Applications/Applications';

function App() {
  return (
    <div className="App">
      <header className={style.header}>
        <img className={style.ApplicationsLogo} src={ApplicationsLogo} alt="Applications"/>
      </header>
      <Applications/>
    </div>
  );
}

export default App;
